# Server Add-Ons for Front-End Developers Tutorial
## The front-end pieces of an add-on

This tutorial aims to explain the various pieces of the Atlassian P2 plugin system that are relevant to developing the front-end of a Server add-on.


## Tutorial

By completing this tutorial, you will:

* Have a basic understanding of the foundations of a Server add-on.
* Understand the flow of your source code in to a compiled Server add-on.
* Be able to push assets to the front-end of an Atlassian Server web product through your add-on.
* Be able to add a new page to an Atlassian Server web product.

Head to the [Wiki of this repository][901] to get started!

## Next steps

Want to know more about what the Atlassian P2 plugin system offers your front-end code at runtime? [Leaning on the WRM at runtime][301] is what you want to do.

Want to learn how to leverage newer versions of JavaScript and modern front-end frameworks when building your add-on? The [compiling the UI tutorial][302] will help you leverage those technologies while building your Server add-on.

Want to build an optimal and performant front-end by taking advantage of a module system for front-end code (for example: ES6 Modules; AMD; UJS; etc.)? Consult the [Bundling the UI tutorial][303].


[1]: https://developer.atlassian.com
[2]: https://developer.atlassian.com/docs/getting-started/set-up-the-atlassian-plugin-sdk-and-build-a-project
[301]: https://bitbucket.org/serverecosystem/sao4fed-the-wrm-at-runtime
[302]: https://bitbucket.org/serverecosystem/sao4fed-compile-the-ui
[303]: https://bitbucket.org/serverecosystem/sao4fed-bundle-the-ui
[901]: https://bitbucket.org/serverecosystem/sao4fed-the-frontend-pieces-of-an-addon/wiki